
## Seu mundo com o GIT - Ferramenta para controle de versões!
***
## Primeiramente veremos uma pequena historia do porque você e quase todo mundo tiveram muitos problemas com ambiente de desenvolvimento e controle de versíonamento a algum tempo atrás, se liga ai.

## Porque o `git` foi inventado?
Para ser mais expressivo e não entrar muito na historia de como e porque o criador da ferramenta fez o que fez, apresento-lhes dois casos de uso para melhor resumir situações encontradas antes das ferramentas de controle de versão em ambientes de desenvolvimento:

 Situação de problema 1: Joãozinho não conseguia voltar alterações em seus arquivos, para isso ele teria que voltar o backup diretamente do servidor ralizando uma serie burocratica de ações e outras coisas algo que levaria muito tempo e sua produção dependia disso.

Situação de problema 2: Imagine que você está desenvolvendo um ERP para supermercados, e existem 3 programadores para realizar as atividades alterando o mesmo arquivo e interferindo diretamente nos documentos que três desenvolvedores acessam ao mesmo tempo. Joãozinho então não conseguia trabalhar em equipe pois sua vida estava virando um inferno ao apagar trechos do codigo de Silas(desenvolvedor do projeto) que o ameaçava de morte sempre que isso acontecia...

  

A partir desses problemas e de varias outroas situações de quse morte, surgiram ferramentas de versionamento e uma delas foi o famoso e formidavel GIT. Obs: Não vou entrar em termos sobre o SVN ou outros.

> Wikipedia do GIT caso lhe interesse: [Clique aqui](https://pt.wikipedia.org/wiki/Git).


## Let`s started!
**Observação sobre o conteudo:** Abaixo listei comandos e observações que explicam na pratica como utilizar as funções da ferramenta git. Utilizei o S.O Windows para realização das atividades.

> Download da ferramenta e metodo de instalação no **link oficial** [Clique aqui](https://git-scm.com/downloads).


## Inicio
### Abaixo segue o escopo do projeto:

* Criar um repositório localmente

* Adicionar (add) e comitar (commit)

* Criar um repositório no GitLab

* Conectar o repositório local com o repositório público

* Sincronizar ambos os repositórios
* Aprender a configurar e enviar commit para o repositorio remoto
* Restaurar branch, reset e revert

## Criar um repositório localmente

Aqui criaremos um repositório no Windows para iniciarmos nosso aprendizado:


1. Criar diretório com o nome a sua escolha, aqui iniciaremos nosso projeto e ramificações com a ferramenta. **Lembre-se:** Utilize diretórios distintos para cada projeto que você inicia, faz com que seu ambiente fique organizado e de fácil entendimento.

2. Baixe o Git no link a seguir: https://git-scm.com/downloads: 
![Versões do git](https://docs.google.com/uc?id=1-OfoslOl7zmPmFlwfGVuRZIE0GABuDPR)
Na imagem podemos ver que existem três versões para download, escolha a correspondente para o seu Sistema Operacional e vamos lá.
3. Para instalação comum, utilize o metodo "Next,next..."/"Finish"
4. Com o git instalado, vamos agora iniciar o **Git Bash Here** no diretório criado para o projeto, dei o nome do meu projeto de ProjetoGit, muito diferenciado né galera kk.
![Ativando o Git Bash Here](https://docs.google.com/uc?id=1-SbBGqWTVyN4HS8DIvK2sYLKpNrbwO81)
### Inicializando o Git
![Git Bash Here](https://docs.google.com/uc?id=1-TDI6Oi3IATVj8w8t6ENUbdXfpoOniyN)
Já visualizando a grandiosa janelinha acima, já adianto que vamos utiliza-la muito então já grave bem a forma de abri-la ok. Booora-la.

## Antes de mais nada!
Para iniciarmos nossa vida aqui, precisamos antes de mais nada nos identificarmos pois iremos trabalhar em equipe e varias pessoas irão atualizar, excluir ou modificar os mesmos dados que você então nada mais justo que cada pessoa criar sua identificação para auxiliar na leitura de logs e integração dentro do projeto.
Basicamente iremos cadastrar nosso nome e e-mail no git para todos saberem que você quem fez as alterações em produção na sexta-feira e deixou a equipe trabalhando até mais tarde para consertar!.
To zuando, kkkk, existe um "comandinho" especial pra você resolver isso caso aconteça e está mais abaixo nesse documento da uma olhada lá caso precise.
Seguindo o fluxo, como cadastramos isso, segue abaixo a lista de comandos e como são muito intuitivos não vou falar sobre eles, somente vou lhes dar o ar da graça de dizer que tudo que você vai fazer abaixo será salvo em um arquivo *conf* dentro do **.git** do seu projeto, então borrrraaa!
4 - Configurar as identificações globais do usuário no GIT para identificação nas alterações:
```
$git config --global user.name "Meu nome não e Jhone"

$git config --global user.email "continuanaosendojhone@gmail.com"

$git config --global core.editor vim,vscode,sublime,....

$git config --list (Mostra todos os dados salvos)
```


## Iniciando de fato a vida no Git
Abaixo temos um comando muito maroto, ele diz ao git para iniciar um projeto nesse diretório. Note que após digitar o comando um diretório oculto *.git* e adicionado dentro do diretório do projeto. Posso ter vários projetos em vários diretórios diferentes iniciando o git dessa forma.
```
$ git init .
```
![Imagem dos diretorios ocultos do git](https://docs.google.com/uc?id=1-ZK6R6ujEENyjO_2CSOl0L6tOR5SVDYf)Aqui temos os arquivos que fazem a maquina funcionar, o config para salvar suas configurações realizadas(vou falar sobre isso ainda), hooks, HEAD e outros. 
[Mais informações sobre os diretórios e para que servem, acesse o link do livro Git e tire suas dúvidas.](https://git-scm.com/book/pt-br/v2/)

## Git ADD
Para que serve e como se usa essa bagaça, a final e o primeiro comando a ser usado depois do *init*, na verdade estou mentindo, seria o *git status* mas segue o fluxo e explico sobre ele posteriormente,  pois bem, aqui vamos adicionar os arquivos a nossa lista de versionamento, a partir de agora nossos arquivos estarão aguardando para serem *comitados*, estamos falando para o git adicionar o conteúdo (arquivos ou diretórios) à um *stage*(estagio no português de Goiás), pode se dizer que aqui falamos pra ele: "Guarda ai pra mim que eu vou usando aqui, mas se eu precisar pego com você de volta". Seria em uma maneira bem horrível de se dizer isso dai, lembrando que quando executo o ADD nada está salvo e versionado ainda, preciso commitar para que o git de fato salve os dados adicionadas com *add* na ferramenta, isso **localmente** salvando em uma **Branch**.

**Mão na massa**
Abaixo temos uma demonstração do comando, note que para que isso funcione você já deve ter arquivos dentro do diretório do projeto para que possamos versionar e notar a funcionalidade da ferramenta, vamos trabalhar com o seguinte arquivo *README.md* (arquivo usado para descrever as funcionalidades do seu projeto e muito mais...). Iremos criar o *README* pois e mais educacional eu acho, mas poderia ser *tereza.txt* ou qualquer outro que-se possa escrever dentro dele.

**Finalmente explico o que o *git status* faz...**
Antes de executar o que esta aqui em baixo caro amigo(a), execute por favor o seguinte comando `$git status`, com ele você verá o que não esta **Tranckeado** ou melhor dizendo, monitorado pela ferramenta, ou seja,  se eu editar meus arquivos e eles não estiverem *Tranckeados(Trancked do inglês) e Tranckeado(Do Goianes)* a ferrameta simplesmente **não irá capturar as alterações realizadas no arquivo ou no diretório do projeto**. 
```
$ git add File.txt (adiciona um arquivo especifico)

$ git add -A (adiciona todos os arquivos/diretorios)
```

**Borá que e hora da revisão:**
Se você criou o arquivo *README.md* ou *joãozinho.txt*, abriu o **Git Bash**, executou o *$git init .* adicionou os arquivos para comitar com o comando * $git add -A* você está no caminho certo.

## Commit, e de comer ou de passar no pé?
Após realizar o *add* dos arquivos, agora e horá gravar e iniciar de fato o versionamento em tudo que fizemos até aqui.
Vamos supor que você esteja em uma equipe de DEV's e precisa deisponibilizar um codigo para a equipe, mas até aqui você só sabe realizar o *git add -A* Enquanto eu não realizar o **commit** os arquivos alterados não serão disponibilizados para o restante da equipe, eles ficaram somente no meu computador.
Então Sebastião, como realizar o bendito do **commit**, segue abaixo:
```
git commit -m "Mensagem do commit"
```
Após executar o *commit* acima você de fato envia para o .git sua versão do diretório atual, mas porque eu tenho que passar uma mensagem no commít?
Faz sentido dizer aos demais de sua equipe e por motivos organizacionais que ajudam você mesmo, dizer porque você apagou ou editou arquivos importantes para sua equipe, com isso você consegue dizer com uma mensagem objetiva e bem elaborada quais arquivos e o que foi modificado dentro do seu projeto. Pense no coletivo.

## Branch (galho ou ramo)
Calma que você não vai plantar nada aqui não, pode deixar!
Na pratica uma branch e uma ramificação da versão principal do seu sistema, note que quando você realizou o commit foi exibido o nome da branch logo em seguida com o nome de **main ou master**

Mais um exemplo de *commit*: Você trabalhou o código e precisa enviar as linhas alteradas de um código ou novos arquivos para o git, onde ele vai salvar apenas o que foi alterado.

**Beleza mas como que eu uso esse negocio moço?**
Listar as branch's existentes:
```
$ git branch
```
Adiciona uma branch
```
$ gti branch NOME_DA_BRANCH
```
Remover uma branch **Warning(tradução seria 'pode dar merda')**
```
$ git branch -D NOME_DA_BRANCH
```
Mudar de uma branch para a outra:
```
$git checkout NOME_DA_BRANCH
```

**É hora da revisão:** Até o presente momento para quem está vivo e ainda não desistiu desse manual meio tosco, vimos até aqui como:

 1. Criar um diretório do projeto e iniciar o git nele.
 2. Cadastrar minhas credenciais no git
 3. Adicionar meus arquivos em um *stage* com o comando `$git add -A`
 4. Realizar um bendito *commit*
 5. Vários comandos para criação de branch
 6. Realizar a mudança de branch's com o checkout.

>**Observação:** Até o momento fizemos tudo isso localmente, ou seja, não saimos da sua maquina.
 - Ue mas você não disse que quando eu fizesse o commit a minha equipe iria conseguir se interar com os meus dados transmitidos?
 - Há resposta para isso e que eu menti!

Até então não temos um local/lugar de destino para onde possamos enviar nossos commits onde nossa equipe ou comunidade irá contribuir ou trabalhar para que o projeto tenha andamento, nesse caso vamos para o próximo item.
***
## Agora, a coisa fica seria !!
>Nesse topico vamos começar a enviar os `commits` para um repositorio remoto, mas antes precisamos saber de algumas coisas que possam ou não te interessar e agregar mais ao seu conhecimento, segue o fluxo!!

Antes de mais nada, o que é um *diretório remoto* no meio do ambiente de controle de versão, como que isso funciona, quais plataformas existem e como posso descobrir, criar ou me interar sobre as funcionalidades?
Booora lá, atualmente existem diversas plataformas que permitem a criação de projetos na internet, como tudo na vida, existem os pagos e os freelas, o *GitHub* por exemplo e uma freelas criada pelo nosso excelentissimo Torvalds, se você não sabe quem é eu te explico, ele so criou um S.O chamado Linux cujo qual existem diversas versões e sabores para todos os gostos e utilidades, pois bem, temos também o GitLab, BitBucket e outras pagas e não pagas.
**Se você não sabe quem criou a ferramenta que você esta usando "GIT" também te conto, dos mesmos criadores de Linux so para baixinhos o grande Torvalds... Heheheheheheheh**

Link do GitHub para criação da sua conta totalmente freelas: https://github.com/

**Projeto Privadou ou Publico?**
Ao se criar o seu primeiro projeto você ficara com essa dúvida, afinal qual escopo usar, aqui vou te explicar com poucas palavras pra passarmos logo dessa parte chata aqui meu.
* **Privado:** Ninguém meche no seu projeto a menos que você permita, lembrando que algumas plataformas não permitem muitos projetos privados, então se atente a isso.
* **Publico:** Seu projeto fica exposta para a comunidade realizar *forcks* e contribuir com ele deixando totalmente *open-source*.

Boooora lá, abaixo tentei ser breve e direto ao ponto, espero que sirva pra ajudar vocês:

Abaixo você tem o '$git add origin' que diz ao git qual será sua nova origem de repositórios, quando você cria um projeto logo na pagina inicial ou na URL você tem o caminho para se chegar até ele, nesse caso vamos adicionar nosso projeto criado no **Git Hub**, aqui deixamos de criar coisas no nosso computador e passamos a enviar nossos dados para um projeto remoto.


>**Observação:** Lembrando que é possível que sua empresa possua repositorios de projetos localmente(em um servidor dentro da empresa), nesse caso o processo e o mesmo, porém você passara o endereço do seu servidor empresarial como caminho de origin.
```
$ git remote add origin https://gitlab.com/rubaogyn/testegit.git (aqui digo ao git que minha nova origem remota e o endereço em sequencia).
```
Para visualizar qual sua origin, você pode utilizar os comandos abaixo:
```
$ git remote 
$ git remote -v
```
>Ambos mostram as informações porem o parametro -v de verbose,lista mais informações na saída do comando.

Quando cadastramos o repositório de origem, e feito dois cadastros automáticos dentro do git, abaixo explico melhor:
```
$ origin https://meulocalgithub/trabalho/ (fetch)
```
>Aqui será de onde eu irei puxar o conteúdo do meu repositório remoto para o meu computador, a origem remota dos meus dados, podendo estar na internet ou em outro computador ou servidor que contenha um endereço remoto.
```
$ origin https://meulocalgithub/trabalho/ (push)
```
> Aqui será o servidor ao qual eu vou empurrar/envia os dados, nesse caso eu vou ser a origem e irei enviar o conteúdo do meu computador para algum destino).

*Finalmente vamos enviar o bendito comitê para o repositório remoto onde de fato todos irão conseguir visualizar minhas alterações no projeto:*

```
$ git push origin master 
```
 Vamos desmistificar o comando acima, aqui estou dizendo o seguinte, envie tudo que estiver na minha branch de nome "master" para minha origin que no caso seria meu repositório remoto, basicamente seria isso, porém vamos aos detalhes:
 - Branchs são ramificações do meu projeto, e dependendo do escopo do seu projeto, você pode ter uma ou varias branchs.
 - Não é uma boa realizar push's direto na branch master, em alguns projetos você tera que requisitar um **pull request** (requisição para realizar enviar os seus dados para o diretório remoto)
 - Você pode criar diversas branchs para o seu projeto, tendo versões ou commits diferentes.
***

## Errei e enviei um commit quebrado para produção sexta-feira as 17:59horas, o que fazer?

**Calma que o papai te explica como resolver**

### Revertendo trabalhos com o REVERT e RESET:

No git temos o REVERT e o RESET para reverter um commit, abaixo vou explicar com minhas palavras como utilizar e para que serve cada um:

 - RESET:

Aqui eu posso usar como parâmetro três opções: **soft, hard e mixed**.

 - **Soft:** Faz com que eu volte para o estado do commit anterior, mas mantenha os arquivos e as alterações realizadas no meu "workdir" local para analise e talvez um novo commit, essa opção e a mais utilizada na horá que há erros e você precisa de uma ação rapida.

 - **Mixed:** Faz o que o SOFT faz porém sobrescreve o seu *stage*, ou seja, os arquivos locais contidos no git serão os arquivos do commit informado ao executar o comando, porém os arquivos do meu *workdir* estarão da mesma forma, no caso do exemplo enviei um codigo quebrado e isso fez com que minha produção seja comprometida, aplicando o Mixed isso será desfeito e o git obtera para o *stage* atual os arquivos restaurados mas no meu *workdir* o codigo errado ainda estará lá para futuras manutenções ou outros.

 - **Hard:** Volta todo o conteudo para o commit anterior sobrescrevendo o "stage" e o "workdir", em poucas palavras, ele sobrescreve o que você tem atualmente no projeto atual, ou seja, se você tiver alterações no codigo elas não serão salvas.
>**Lembrando que:** O HEAD seria como um ponteiro, onde eu aponto para o commit que eu quero alterar ou restaurar, por isso tenho que listar os commits para saber para onde ir, abaixo explicarei como listar os commits realizados.

#### Parou tudo, um ponto importante é que precisamos listar os commits para saber onde ir e o que restaurar;
Para visualizar a sua lista de commits, utilize o comando:
```
$ git log
```
>Com o git log eu consigo visualizar todos os commits realizados e informações como: Mensagem do commit, Autor, data, hora e o princiapal que é a identificação do commit, seu identificador de 'hash'.

Para utilizarmos o comando RESET precisamos das informações contidas no commit, nesse caso utilizamos o `$git log` para nos darmos essa informação, porém o codigo de identificação e muito grande e complexo, nesse caso vamos utilizar apenas os 7 primeiros digitos para realizarmos nossa operação que ficaria assim:
```
$ git reset --soft SETE_PRIMEIROS_DIGITOS_DO_HASH
```

 - REVERT:

No Git REVERT chamado de salva sexta-feira, porque caso o codigo enviado de algum problema ele retorna tudo para o estado do ultimo commit realizado, por exemplo, enviei o commit A para produção e descobri que o codigo estava errado e que arquivos foram faltando e a produção quebrou, nesse caso ao utilizar o revert ele vai utilizar o commit B e substituir pelo A, porém o commit continua na lista de logs para utilização caso você queira consertar ou editar o codigo contido ali.
```
$ git revert --no-edit ID_DO_COMMIT_QUE_DEU_RUIM
```
 >Aqui ele ira voltar o commit anterior ao enviado no comando, ou seja, ao estado que estava antes do commit ser gerado.

***  
###### Deletenado uma Branch LOCAL ou REMOTA

**Para deletar branch's remotamente você utiliza o comando abaixo:**
```
$ git push origin --delete Nome_da_branch
```
>Aqui você deleta branchs remotas

##### Para deletar branchs locais você utiliza o comando abaixo:
```
$ git branch -D Nome_da_branch
```
>Aqui você deleta uma branch criada localmente.

***
###### FAMOSO GIT PULL / AtualizarDiretoriosLocais

**GIT PULL -> Simplismente baixa o que estiver na sua branch remota para a branch local, sincronizando os diretorios.**

>**Lembre-se:** E uma boa pratica sempre realizar um git pull antes de iniciar as alterações na branch.
```
$ git pull origin NOME_DA_BRANCH
```
***
 ###### FORKED / GIT HUB

Realiza o clone do projeto para sua conta, ou seja, ele clona o projeto para ser alterado por você.

Após o termino você realiza o push das informações e realizando o também famoso "PULL REQUEST" ou "MERGE REQUEST" para o dono do projeto principal, onde ele decidira se as alterações realizadas por você serão ou não implementadas no projeto.

***
###### GIT CLONE / CLONANDO REPOSITORIOS

Com a função `git clone` eu posso clonar um repositório e realizar alterações ou melhorias para auxiliar em determinados projetos, com ele eu realizo uma copia do projeto original para meu repositorio local.
```
$ git clone https://github.com/Rubensfp/juice-shop.git
```

>Chegamos ao final da jornada, espero que tenha achado algo util aqui e se não achou adicione algo que ache relevante realizando um `fork`do projeto. **Vamos crescer juntos!**

***

***A seguir você verá alguns comandos mais utilizados no dia-a-dia com uma explicação breve***

*Comandos mais usados no dia a dia:*

```
$ git init
```
>Inicia o diretorio do git, adicionando um diretorio oculto onde existem as configurações da ferramenta.


```
$ git add -a / git add file.txt
```
> Adiciona um arquivo especifico ou todos, para o git deixando no formato tracken ou monitorado.
```
$ git commit -m "" 
```
>Commita o arquivo, o commit faz um snapshot dos arquivos naquele momento.

```
$ git commit -am ""
```
> Comita e adiciona todos os arquivos para serem monitorados como Tracked, não sendo necessãrio o add -A.

```
$ git reset -soft
```
> Faz com que eu volte para o estado do commit anterior, mas mantenha os arquivos e as alterações realizadas no meu git local para analise e talvez um novo commit.

```
$ git reset --hard
```
> Volta o commit inteiro, volta totalmente para o estado do commit alterado.

```
$ git reset --soft
```
> Volta o commit com os arquivos inteiros e todas as alterações no momento do commit.

```
$ git diff
```
> Diferencia os arquivos, você visualiza qual a alteração realizada dentro dos arquivos.

```
$ git diff --name-only 
```
>Lista como um ls, listando os arquivos que foram alterados no diretório.

```
$ git diff README.md
```
> Mostra o que foi alterado apenas no README.md
```
$ git checkout HEAD -- nomedoarquivo.html 
```
>Supondo que você errou e apagou uma linha sem querer do arquivo, aqui você pode retornar o arquivo para o estado do ultimo commit, passando o nome do arquivo especifico, lembrando que o HEAD sinaliza que o arquivo será retornado do commit atual situado na branch atual.
***
Caso precise, como material de conhecimento aconselho sempre ter na área de trabalho o CHEAT SHEET do GIT, segue o link para download: [CHEAT-SHEET](https://education.github.com/git-cheat-sheet-education.pdf)


#### End...